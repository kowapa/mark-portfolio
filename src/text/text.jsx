import { StyledText } from "./styles";

export default function Text(props) {
  return (
    <StyledText
    color={props.color}
    flex={props.flex}
    weight700={props.weight700}
    right={props.right}
    lettersp4={props.lettersp4}
    $fontSize={props.$fontSize}
    $mbe={props.$mbe}
    $mbs={props.$mbs}
    $margin={props.$margin}
    $width={props.$width}
    $padding={props.$padding}
    >{props.children}</StyledText>
  );
}