import React from "react";
import '../../App.css';
import belkav from '../../assets/blstrelka.svg';
import {Wrapper, TextHolder, Blog, Right, Button, Photo, Heading, SLink} from './styles';

const Counter = () => {
    const allblogs = [
        {
           title: 'April 16, 2021 . 6 mins',
           text: 'Design tips for designers, that cover everything you need',
           btn: 'Read the article',
           btnimg: belkav
        },
        {
          title: 'April 16, 2021 . 15 mins',
          text: 'How to build rapport with your web design clients',
          btn: 'Read the article',
          btnimg: belkav
        },
        {
          title: 'April 16, 2021 . 5 mins',
          text: 'Top 6 free website mockup tools 2021',
          btn: 'Read the article',
          btnimg: belkav
        },
        {
          title: 'April 16, 2021 . 17 mins',
          text: 'Logo design trends to avoid in 2021',
          btn: 'Read the article',
          btnimg: belkav
        },
        {
          title: 'April 16, 2021 . 7 mins',
          text: '22 best UI design tools',
          btn: 'Read the article',
          btnimg: belkav
        }
      ]
    return(
        <Wrapper id='blog' item xs={12} sx={{ height: { xs: '1950px', md: '1380px' }}}>
        <TextHolder item xs={10} sx={{ padding: { xs: '70px 40px', md: '250px 82px' }}}>
          <Heading color="#FFA84B" letterSpacing="4px" $bold item xs={12} sx={{ textAlign: { xs: 'center', md: 'left' }}}>BLOGS</Heading>
            <Right item xs={10} sx={{ float: { xs: 'auto', md: 'right' }}}>
              {allblogs.map(box => (
                <Blog key={box.title} item xs={10} sx={{ width: { xs: '190px', md: '630px' }}}>
                    <Heading color="#b3b3b3" fontSize="15px" $bold $mbs="15px" $mbe="15px">{box.title}</Heading>
                    <Heading color="#fff" fontSize="28px" $bold>{box.text}</Heading>
                  <Button>
                    <SLink href="/">{box.btn}<Photo $margin="0px 0px 0px 5px" src={box.btnimg} alt="arrow"/></SLink>
                  </Button>
                  <hr/>
              </Blog>
              ))}
            </Right>
            <Heading color="#fff" $bold fontSize="54px" margin="25px 0px" item sx={{ textAlign: { xs: 'center', md: 'left' }}}>Latest Blogs</Heading>
            <SLink item sx={{ display: { xs: 'flex', md: 'block' }, justifyContent: { xs: 'center', md: 'auto' }}} href='/'>View all<Photo $margin="0px 0px 0px 5px" src={belkav} alt="arrow"/></SLink>
        </TextHolder>
      </Wrapper>
    );
};
export default Counter;