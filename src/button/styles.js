import { Button } from '@mui/material';
import styled from 'styled-components';

export const StyledButton = styled(Button)`
    all: unset;
    padding: 20px 30px;
    text-decoration: none;
    letter-spacing: 0px;
    border: 2px solid white;
    font-size: 18px;
    font-weight: 400;   
    &:hover{
        color: #000;
        border: 2px solid black;
        cursor: pointer;
    }
    color: ${props => props.$color ? props.$color : "#fff"};
    background: ${props => props.$background ? props.$background : "#000"};
    margin-left: ${props => props.$marginl ? props.$marginl : "none"};
    float: ${props => props.$float ? props.$float : ""};
`;