import React from "react";
import '../../App.css';
import poduct1 from '../../assets/product1.png';
import poduct2 from '../../assets/product2.png';
import poduct3 from '../../assets/product3.png';
import poduct4 from '../../assets/product4.png';
import {Productdes, TextHolder, ProductImg, RightImg, Heading, Photo} from './styles';
import {Grid} from '@mui/material';

const Counter = () => {
    return(
       <Productdes>
        <TextHolder item xs={12} sx={{flexDirection: {xs: 'column-reverse', md: 'row-reverse'}}}>
          <Grid item sx={{ width: { xs: '350px', md: '460px' }}}>
            <Heading $float="float" color="#666666" fontSize="20px">Over the past 12 years, I've worked with a diverse range of
            clients, from startups to Fortune 500 companies. I love crafting
            interfaces that delight users and help businesses grow.</Heading>
          </Grid>
          <Grid fontSize="18px">
            
              <Heading color="#FF5E69" letterSpacing="4px" $bold fontSize="22px" item sx={{ textAlign: { xs: 'center', md: 'none' }}}>PRODUCT DESIGNER</Heading>
            
              <Heading fontSize="54px" $bold>That's me!</Heading>
            
          </Grid>
        </TextHolder>
        <ProductImg item xs={12} sx={{display: {xs: 'none', md: 'flex'}}}>
          <Photo $margin="10px" src={poduct1} alt="phone"/>
          <Photo $margin="10px" src={poduct2} alt="pc"/>
          <RightImg>
            <Photo $margin="10px" src={poduct3} alt="macbook"/>
            <Photo $margin="10px" src={poduct4} alt="work_with_macbook"/>
          </RightImg>
        </ProductImg>
      </Productdes>
    );
};
export default Counter;