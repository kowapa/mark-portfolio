import styled from 'styled-components';
import {Box, Typography, Link} from '@mui/material';

export const Wrapper = styled(Box)`

`

export const TextHolder = styled(Box)`
    display: flex;
    flex-direction: column;
`

export const Button = styled(Link)`
    display: flex;
    flex-direction: column;
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
`

export const Right = styled(Typography)`
    float: right;
`

export const Photo = styled.img`

`