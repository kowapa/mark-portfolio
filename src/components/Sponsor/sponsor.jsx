import React from "react";
import '../../App.css';
import logoipum1 from '../../assets/logoipum1.svg';
import logoipum2 from '../../assets/logoipum2.svg';
import logoipum3 from '../../assets/logoipum3.svg';
import logoipum4 from '../../assets/logoipum4.svg';
import {Sponsor, TextHolder, ImgHolder, Heading, Photo} from './styles';
import { Grid } from "@mui/material";

const Counter = () => {
    return(
      <Grid item xs={12} sx={{display: {xs: 'none', md: 'block'}}}>
       <Sponsor>
          <TextHolder>
            <Heading>Trusted by</Heading>
          </TextHolder>
          <ImgHolder>
            <Photo src={logoipum1} alt="logoipum"/>
            <Photo src={logoipum2} alt="logoipum"/>  
            <Photo src={logoipum3} alt="logoipum"/>  
            <Photo src={logoipum4} alt="logoipum"/>
          </ImgHolder>
        </Sponsor>
      </Grid>
    );
};
export default Counter;