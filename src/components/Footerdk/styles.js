import styled from 'styled-components';
import {Box, Typography, Link} from '@mui/material';

export const Wrapper = styled(Box)`
    padding: 0px 0px; 
    background: #000; 
    color: #fff;
`

export const FooterInfo = styled(Box)`
    display: flex;
    flex-direction: column;
    align-content: flex-start;
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
    float: ${props => props.$right ? 'right' : ''};
    margin-block-end: ${props => props.$mbe ? props.$mbe : 0};
    margin-block-start: ${props => props.$mbs ? props.$mbs : 0};
`

export const AllButtons = styled(Box)`
    float: right;
    width: 640px;
`

export const Button = styled(Link)`
    text-decoration: none;
    display: flex;
    color: #fff;
    float: right;
    flex-direction: column;
    align-items: flex-start;
    width: 200px; 
    height: 60px; 
    font-weight: 700; 
    justify-content: center;
    font-size: 16px;
    cursor: pointer;
`

export const Photo = styled.img`
    margin: ${props => props.$margin ? props.$margin : ""}; 
    padding: ${props => props.$padding ? props.$padding : ""};
`
export const Footer = styled.footer`
    
`;
