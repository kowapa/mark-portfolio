import styled from 'styled-components';
import { Field, Form } from 'formik';
import {Grid, Typography} from '@mui/material';

export const SForm = styled(Form)`
    display: flex;
    flex-direction: column;
    align-content: center;
    align-items: center;
`

export const Account = styled(Grid)`
  text-align: center;
`

export const Title = styled.h1`
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 60px 0px 20px;
`    

export const SLabel = styled.label`
    margin: 10px 0px 5px;
`

export const SField = styled(Field)`
    padding: 5px 14px;
    border-radius: 20px;
    border-color: #0000;
    background: #efefef;
`

export const Text = styled(Typography)`
    margin-block-start: 10px;
    margin-block-end: 10px;
    line-height: 1.5;
`
