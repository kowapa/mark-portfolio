import React from "react";
import '../../App.css';
import men2 from '../../assets/men.png'
import kav from '../../assets/kavicki.svg'
import leftwhite from '../../assets/leftwhite.svg'
import rightwhite from '../../assets/rightwhite.svg'
import {Wrapper, TextHolder, Right, Button, Heading, Photo} from './styles';
import {Grid} from '@mui/material';

const Counter = () => {
    return(
       <Wrapper item xs={12} sx={{padding: {xs: '100px 82px 670px', md: '100px 82px'}}}>
        <TextHolder>
          <Heading color="#4353FF" $bold fontSize="22px" letterSpacing="4px">TESTIMONIALS</Heading>

          <Heading $bold item sx={{fontSize: {xs: '44px', md: '54px'}}}>Word on the street</Heading>
        </TextHolder>

        <Grid padding="100px 0px 0px 0px">
          <Right>
            <Photo src={kav} alt="img"/>

            <Grid item xs={10} sx={{width: {xs: '285px', md: '565px'}}}>
              <Heading $bold fontSize="36px" margin="50px 0px 30px">Jade helped us build a software so 
              intuitive that it didn't need a 
              walkthrough. He solved complex 
              problems with brilliant design.</Heading>
            </Grid>
            <Right item xs={2} sx={{display: {xs: 'none', md: 'block'}}}>
              <Button href="/"><Photo src={leftwhite} alt="arrow"/></Button>
              <Button href="/"><Photo src={rightwhite} alt="arrow"/></Button>
            </Right>
            <Heading fontSize="20px" $bold>John Frankin</Heading>
            <Heading fontSize="18px">Founder, Double Bunch</Heading>
          </Right>
          <Grid item xs={6} sx={{display: {xs: 'none', md: 'block'}}}><Photo  src={men2} alt="men"/></Grid>
        </Grid>
      </Wrapper>
    );
};
export default Counter;