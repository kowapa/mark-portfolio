import styled from 'styled-components';
import {Box, Typography} from '@mui/material';

export const Wrapper = styled(Box)`
    padding: 100px 82px;
    background: #000;
`

export const TextHolder = styled(Box)`
    display: flex; 
    align-items: center; 
    justify-content: center;
    flex-direction: column;
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
    float: ${props => props.$right ? 'right' : ''};
    margin-block-end: ${props => props.$mbe ? props.$mbe : 0};
    margin-block-start: ${props => props.$mbs ? props.$mbs : 0};
`