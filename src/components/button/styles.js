import { Button } from '@mui/material';
import styled from 'styled-components';

export const StyledButton = styled.button`
    all: unset;
    text-decoration: none;
    letter-spacing: 0px;
    border: 2px solid white;
    font-size: 18px;
    font-weight: 400;   
    &:hover{
        color: #000;
        border: 2px solid black;
        cursor: pointer;
        background: #fff;
    }
    border-radius: ${props => props.$borderRadius ? props.$borderRadius : ""};
    margin: ${props => props.$margin ? props.$margin : ""};
    padding: ${props => props.$padding ? props.$padding : "20px 30px"};
    color: ${props => props.$color ? props.$color : "#fff"};
    background: ${props => props.$background ? props.$background : "#000"};
    margin-left: ${props => props.$marginl ? props.$marginl : ""};
    float: ${props => props.$float ? props.$float : "none"};
`;