import Accordion from '@mui/material/Accordion';
import styled from 'styled-components';

export const StyledAccordion = styled(Accordion)`
    background: #000;
    width: 50%;
    height: 135px;
    color: #fff;
    flex-wrap: nowrap | wrap | wrap-reverse;
    float: ${props => props.right ? "right" : "none"};
`;
