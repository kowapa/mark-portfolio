import React from "react";
import '../../App.css';
import img1 from '../../assets/1img.svg';
import img2 from '../../assets/2img.svg';
import img3 from '../../assets/3img.svg';
import {Services, TextHolder, Heading, Photo, Ul, Li} from './styles';
import {Grid} from '@mui/material';

const Counter = () => {
    const boxes = [
        {
          img: img1,
          title: 'What I can do for you',
          text: 'Faster, better products that your users love. Heres all the services I provide:',
          list: [
            'Design Strategy',
            'Web and Mobile App Design',
            'Front-end Development'
          ]
        },
        {
          img: img2,
          title: 'Applications Im fluent in',
          text: 'Every designer needs the right tools to do the perfect job. Thankfully, Im multilingual.',
          list: [
            'Sketch',
            'Webflow',
            'Figma'
          ]
        },
        {
          img: img3,
          title: 'What you can expect',
          text: 'I design products that are more than pretty. I make them shippable and usable.',
          list: [
            'Clean and functional',
            'Device and user friendly',
            'Efficient and maintainable'
          ]
        }
      ]
    return(
       <Services id='services'>
          <TextHolder>
            <Grid display="flex" justifyContent="center" >
              <Heading color="#FF5E69" $bold fontSize="22px" letterSpacing="4px">SERVICES</Heading>
            </Grid>
            <Grid item xs={12} sx={{textAlign: {xs: '', md: 'center'}, width: {xs: '290px', md: '770px'}}}>
              <Heading $bold item xs={12} sx={{fontSize: {xs: '32px', md: '54px'}}}>Design that solves problems, one product at a time.</Heading>
            </Grid>
          </TextHolder>

          <Grid display="flex" justifyContent="center" alignItems="center" item xs={12} sx={{flexDirection: {xs: 'column-reverse', md: 'row'}}}>
            {boxes.map(box => (
              <Grid key={box.title} padding="25px 60px" width="405px">
                <Photo src={box.img} alt="logo"/>
                <Heading fontSize="24px" $bold $mbs="15px" $mbe="15px">{box.title}</Heading>
                <Grid width="230px" color="#666666">
                  <Heading fontSize="15px">{box.text}</Heading>
                </Grid>
                <Ul>
                  {box.list.map(point =>(
                    <Li key={point}>{point}</Li>
                  ))}
                </Ul>
              </Grid>
            ))}
          </Grid>
      </Services>
    );
};
export default Counter;