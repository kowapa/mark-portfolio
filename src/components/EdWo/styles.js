import styled from 'styled-components';
import {Box, Typography} from '@mui/material';

export const Wrapper = styled(Box)`
    justifyContent: center; 
    display: flex; 
    padding: 100px 10px;
`

export const WorkExperience = styled(Box)`
    float: right; 
    display: flex; 
    clear: both; 
    padding: 0px 30px;
`

export const Wore = styled(Box)`
    width: 650px;
    margin: 10px 0px; 
    height: 100px;
`

export const Arrow = styled(Box)`
    float: right;
`
export const Education = styled(Box)`
    padding: 0px 30px;
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
    float: ${props => props.$right ? 'right' : ''};
    margin-block-end: ${props => props.$mbe ? props.$mbe : 0};
    margin-block-start: ${props => props.$mbs ? props.$mbs : 0};
`

export const Photo = styled.img`

`