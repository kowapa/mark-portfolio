import { StyledButton } from "./styles";

export default function Button(props) {
  return (
    <StyledButton
    $color={props.$color}
    $background={props.$background}
    $marginl={props.$marginl}
    $float={props.$float}
    onClick={props.onClick}
    >{props.children}</StyledButton>
  );
}