import styled from 'styled-components';
import Typography from '@mui/material/Typography';

export const StyledText = styled(Typography)`
    line-height: unset;
    display: ${props => props.flex ? "flex" : ""};
    font-weight: ${props => props.weight700 ? "700" : "400"};
    float: ${props => props.right ? "right" : ""};
    letter-spacing: ${props => props.lettersp4 ? "4px" : 0};
    font-size: ${props => props.$fontSize ? props.$fontSize : '22px'};
    margin-block-end: ${props => props.$mbe ? props.$mbe : 0};
    margin-block-start: ${props => props.$mbs ? props.$mbs : 0};
    margin: ${props => props.$margin ? props.$margin : 'none'};
    width: ${props => props.$width ? props.$width : ''};
    padding: ${props => props.$padding ? props.$padding : ''};
`