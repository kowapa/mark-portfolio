import styled from 'styled-components';
import {Box, Typography, Link} from '@mui/material';

export const Wrapper = styled(Box)`
    background: #000;
`

export const TextHolder = styled(Box)`
    
`

export const Blog = styled(Box)`
    padding: 0px 82px;

`

export const Button = styled(Box)`
    margin: 30px 0px;
`

export const Right = styled(Box)`

`

export const SLink = styled(Link)`
    text-decoration: none;
    color: #fff;
`

export const Photo = styled.img`
    margin: ${props => props.$margin ? props.$margin : ""};
`
export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
    margin-block-end: ${props => props.$mbe ? props.$mbe : 0};
    margin-block-start: ${props => props.$mbs ? props.$mbs : 0};
`