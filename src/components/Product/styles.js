import styled from 'styled-components';
import {Box, Typography} from '@mui/material';

export const Productdes = styled(Box)`
    padding: 100px 0px;
`

export const TextHolder = styled(Box)`
    display: flex;
    justify-content: space-around;
    align-items: center; 
    padding: 0px 52px;
`

export const ProductImg = styled(Box)`
    padding: 150px 0px 0px 0px;
    justify-content: center;
    clear: both;
`

export const RightImg = styled(Box)`
    float: right;
    display: flex;
    flex-direction: column;
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
    float: ${props => props.$float ? props.$float : ''};
`

export const Photo = styled.img`
    margin: ${props => props.$margin ? props.$margin : ''};
`