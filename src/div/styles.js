import styled from 'styled-components';

export const StyledDiv = styled.div`
    display: ${props => props.$display ? props.$display : ""}; 
    padding: ${props => props.$padding ? props.$padding : ""};
    margin: ${props => props.$margin ? props.$margin : ""};
    letter-spacing: ${props => props.$letterSp ? props.$letterSp : ""};
    align-items: ${props => props.$alignItems ? props.$alignItems : ""};
    justify-content: ${props => props.$justifyContent ? props.$justifyContent : ""};
    width: ${props => props.$width ? props.$width : ""};
    color: ${props => props.$color ? props.$color : ""};
    font-weight: ${props => props.boldFont ? "700" : "400"};
    font-size: ${props => props.$fontSize ? props.$fontSize : ""};
    flex-direction: ${props => props.$flexDirection ? props.$flexDirection : ""};
    z-index: ${props => props.$zIndex ? props.$zIndex : ""};
    margin-block-end: ${props => props.$mbe ? props.$mbe : ""};
    margin-block-start: ${props => props.$mbs ? props.$mbs : ""};
    float: ${props => props.$float ? props.$float : ""};
    height: ${props => props.$height ? props.$height : ""};
    background: ${props => props.$background ? props.$background : ""};
    clear: ${props => props.$clear ? props.$clear : ""};
    position: ${props => props.$position ? props.$position : ""};
    text-align: ${props => props.$textAlign ? props.$textAlign : ""};
`