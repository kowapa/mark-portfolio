import React, { useState } from 'react';
import men from '../../assets/men.jpg';
import Button from '../button/button';
import {Wrapper, TextHolder, Heading, Photo} from './styles';
import {Grid} from '@mui/material';

const Counter = (props) => {
    const [name, setName] = useState('Johny');
    return(
      <>
      <Wrapper id='about' item xs={12} sx={{ padding: { xs: '50px 10px 100px 10px;', md: '180px 82px 100px 82px' }}}>
        <Grid container alignItems="flex-end" justifyContent="flex-start">
          <Grid item xs={12} md={8}>
            <TextHolder>
              <Heading variant="h2" $bold color="#FF8A56" item sx={{fontSize: {xs: '30px', md: '60px'}}}>I design products</Heading>
              <Heading variant="h2" $bold item sx={{fontSize: {xs: '30px', md: '60px'}}}>that delight and inspire people.</Heading>
              <Heading color="#666666" item sx={{fontSize: {xs: '20px', md: '30px'}, width: {xs: '300px', md: 'auto'}}}>Hi! I’m {name}, a product designer based in Berlin. I create user-friendly interfaces for fast-growing startups.</Heading>
            </TextHolder>
              <Button onClick={() => props.setShow(false)}>Book a call</Button>
              <Button $color="#000" $background="#fff" item xs={4}>Download CV</Button>
          </Grid>
          <Grid item xs={4} sx={{ display: { xs: 'none', md: 'flex' }}}>
            <Photo src={men} alt="my photo"/>    
          </Grid>
        </Grid>
      </Wrapper>
      </>
    );
};
export default Counter;