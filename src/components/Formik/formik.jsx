import React, { useState } from 'react';
import { Formik, Field, Form } from 'formik';
import {Title, SForm, SLabel, SField, Text, Error, Account} from './styles';
import {Grid} from '@mui/material';
import Button from '../button/button';
import {SignupSchema} from '../../validation';
import { TextField } from 'formik-mui';

const Counter = () => {
    const [formValues, setFormValues] = useState({});
    const [isSubmitted, setIsSubmitted] = useState(false);

    return(
        <Grid height="700px">
            <Formik
            initialValues={{
              firstName: "",
              email: "",
              phone: "",
              topics: "",
              message: "",
            }}
            validationSchema={SignupSchema}
            onSubmit={(values) => {
              setIsSubmitted(true);
              setFormValues(values);
            }}
            >
            {!isSubmitted ? (<> 
              <Title>Sign Up</Title>
            <SForm>
                <SLabel htmlFor="firstName">First Name</SLabel>
                <SField component={TextField} id="firstName" name="firstName" placeholder="Jane" />

                <SLabel htmlFor="email">Email</SLabel>
                <SField component={TextField} id="email" name="email" placeholder="jane@acme.com" type="email" />

                <SLabel htmlFor="phone">Phone number</SLabel>
                <SField component={TextField} id="phone" name="phone" placeholder="20089550" />

                <SLabel htmlFor="topics">Topics</SLabel>
                <SField component={TextField} id="topics" name="topics" placeholder="topics" />

                <SLabel htmlFor="message">Message</SLabel>
                <SField component={TextField} id="message" name="message" placeholder="Hi my name Mark" />

                <Button $padding="5px 25px" $borderRadius="40px" $margin="20px" type="submit">Submit</Button>
            </SForm>
            </>) : (
              <Account id="account">
                    <Text>
                        Hi, {formValues.firstName}!
                        You Sign Up on Portfolio Creator.
                    </Text>
                </Account>
            )}
            </Formik>

        </Grid>
    );
};
export default Counter;