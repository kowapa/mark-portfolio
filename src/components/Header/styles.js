import styled from 'styled-components';
import {Typography, Link} from '@mui/material';

export const Ul = styled.ul`
    display: flex; 
    justify-content: center;
    align-items: center; 
    margin-block-start: 0px;
`

export const Li = styled.li`
    margin-left: 20px;
    list-style: none;
`

export const Button = styled(Link)`
    text-decoration: none;
    font-size: 16px;
    cursor: pointer;
    padding: 6px 15px;
    border-radius: 20px;
    color: #000; 
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
`

export const Photo = styled.img`

`

export const Header = styled.header`
    position: fixed;
    top: 0;
    left: 0;
    width: 94%;
    display: flex;
    align-items: center;
    padding: 50px 82px;
    z-index: 10000;
    justify-content: space-between;
    background: #fff;
`;
