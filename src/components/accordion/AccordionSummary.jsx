import AccordionSummary from '@mui/material/AccordionSummary';
import styled from 'styled-components';

export const StyledAccordionSummary = styled(AccordionSummary)`
    min-height: 40px;
`;