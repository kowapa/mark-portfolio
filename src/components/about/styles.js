import styled from 'styled-components';
import {Box, Typography} from '@mui/material';
import theme from '../../theme';

export const Wrapper = styled(Box)`
    display: flex;
    align-items: center;
    justify-content: space-between;
    letter-spacing: 2px;
`

export const TextHolder = styled(Box)`
    width: 100%;
    margin-bottom: 20px;
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
`

export const Photo = styled.img`

`