import styled from 'styled-components';
import {Box, Typography} from '@mui/material';

export const Services = styled(Box)`
    display: flex;
    padding: 60px 0px;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`

export const TextHolder = styled(Box)`
    padding: 0px 0px 100px 0px;
`

export const Ul = styled.ul`
    padding-inline-start: 5px;
`

export const Li = styled.li`
    list-style-type: square;
    font-sizez: 18px; 
    font-weight: 700;
    margin: 10px 0px;
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
    margin-block-end: ${props => props.$mbe ? props.$mbe : 0};
    margin-block-start: ${props => props.$mbs ? props.$mbs : 0};
`

export const Photo = styled.img`

`