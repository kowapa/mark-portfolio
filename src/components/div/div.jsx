import { StyledDiv } from "./styles";

export default function Div(props) {
  return (
    <StyledDiv
    color={props.$color}
    $display={props.$display}
    boldFont={props.boldFont}
    $float={props.$float}
    $letterSp={props.$letterSp}
    $fontSize={props.$fontSize}
    $mbe={props.$mbe}
    $mbs={props.$mbs}
    $margin={props.$margin}
    $padding={props.$padding}
    $alignItems={props.$alignItems}
    $justifyContent={props.$justifyContent}
    $width={props.$width}
    $flexDirection={props.$flexDirection}
    $zIndex={props.$zIndex}
    $height={props.$height}
    $background={props.$background}
    $clear={props.$clear}
    $position={props.$position}
    $textAlign={props.$textAlign}
    >{props.children}</StyledDiv>
  );
}