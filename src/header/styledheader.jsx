import { StyledHeader } from "./styles";

export default function Header(props) {
  return (
    <StyledHeader
    >{props.children}</StyledHeader>
  );
}