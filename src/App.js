import './App.css';
import Header from './components/Header/header';
import About from './components/about/about';
import Sponsor from './components/Sponsor/sponsor';
import Services from './components/Services/services';
import Projects from './components/Projects/projects'
import Blog from './components/Blog/blog';
import Product from './components/Product/productdes';
import EdWo from './components/EdWo/EdWo';
import Testimonials from './components/TestiMonials/testimonials';
import Faq from './components/Faq/faq';
import Formik from './components/Formik/formik'
import Footerdk from './components/Footerdk/footerdk';
import './App.css';
import { useState } from 'react';
import * as React from 'react';

export default function App() {
  const [show, setShow] = useState(true);
  return (
    <div className="App">
      <Header/>
      <About setShow={()=>setShow()}/>
      {
        show?<Sponsor/>:null
      }
      <Services/>
      <Projects/>
      <Blog/>
      <Product/>
      <EdWo/>
      <Testimonials/>
      <Faq/>
      <Formik/>
      <Footerdk/>
    </div>
  );
}