import React from "react";
import '../../App.css';
import logo2 from '../../assets/logo2.png';
import email from '../../assets/email.svg';
import {Wrapper, FooterInfo, Heading, AllButtons, Button, Photo, Footer} from './styles';
import {Grid} from '@mui/material';

const Counter = () => {
    return(
       <Wrapper>
        <Footer>
          <FooterInfo>
            <Grid margin="0px 0px 50px 0px">
                <Heading color="#fff" $bold fontSize="54px" $mbs="140px" padding="0px 82px">Ready to make something kickass?</Heading>

                <Heading color="#3C46FF" $bold fontSize="54px" $mbs="0px" padding="0px 82px">Let's get on a call.</Heading>
            </Grid>
            <Grid padding="0px 82px">
            <AllButtons item xs={10} sx={{width: {xs: '400px', md: '640px'}, float: {xs: 'auto', md: 'right'}}}>
              
                  <Button href='/'>Experience</Button>
                  <Button href='/'>Services</Button>
                  <Button href='/'>About</Button>
                  
                  <Button href='/'>Projects</Button>
                  <Button href='/'>Blog</Button>
                  <Button href='/'>Contact</Button>
                 
                  <Button href='/'>Twitter</Button>
                  <Button href='/'>Instagram</Button>
                  <Button href='/'>Dribbble</Button>

              </AllButtons>
                <Photo src={logo2} alt="logo"/>

                <Heading color="#666666" fontSize="16px">4353 Delaware Avenue, San Francisco, USA</Heading>

                <Heading color="#5C5C5C" fontSize="16px" $mbs="15px"><Photo margin="0px 10px 0px 0px" src={email} alt="email"/>hi@thefolio.com</Heading>

                <Heading color="#8A95AD" fontSize="17px" margin="0px" padding="250px 0px 10px 0px">© All rights reserved. Sumit Hegde . Powered by Webflow / Image License Info / Instructions / Changelog / Style Guide</Heading>

            </Grid>
          </FooterInfo>
        </Footer>
      </Wrapper>
    );
};
export default Counter;