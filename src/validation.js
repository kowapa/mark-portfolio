import * as Yup from 'yup';

export const SignupSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Required'),
    email: Yup.string().email('Invalid email').required('Email required'),
    phone: Yup.string().required('Enter phone number'),
    topics: Yup.string().required('Provide topic').max(50, 'Too Long!'),
    message: Yup.string().required('Please write me a message').max(150, 'Too Long!'),
  });