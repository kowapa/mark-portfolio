import styled from 'styled-components';

export const StyledHeader = styled.header`
    position: fixed;
    top: 0;
    left: 0;
    width: 94%;
    display: flex;
    align-items: center;
    padding: 50px 82px;
    z-index: 10000;
    justify-content: space-between;
    background: #fff;
`;