import React from "react";
import '../../App.css';
import logo from '../../assets/IMAGE.svg';
import stel from '../../assets/strelka.svg';
import {Ul, Li, Button, Photo, Header} from './styles';
import { Grid } from "@mui/material";

const Counter = () => {
    return(
        <Grid item xs={12} sx={{display: {xs: 'none', md: 'flex'}}}>
            <Header>
                <Photo src={logo} alt="logo"/>
                <Ul>
                    <Li><Button href="#about">About</Button></Li>

                    <Li><Button href="#services">Services</Button></Li>

                    <Li><Button href="#projects">Projects</Button></Li>

                    <Li><Button href="#blog">Blog</Button></Li>

                    <Li><Button href="#bookacall">Book a call<Photo src={stel} alt="arrow"/></Button></Li>
                </Ul>
            </Header>
        </Grid>
    );
};
export default Counter;