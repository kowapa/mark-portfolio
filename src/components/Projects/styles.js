import styled from 'styled-components';
import {Box, Typography, Link} from '@mui/material';

export const Projects = styled(Box)`
    display: flex;
    justify-content: space-between;
    flex-direction: column; 
    z-index: 100;
    padding: 110px 0px 0px 0px;
`

export const TextHolder = styled(Box)`
    padding: 0px 82px 370px 82px;
`

export const Title = styled(Box)`
    font-size: 22px;
    letter-spacing: 4px;
    font-width: 700; 
    margin-block-end: 0px;
`

export const ProjWallp = styled(Box)`
    display: flex;
    position: absolute;
    margin: 230px 0px 0px 67px; 
    align-items: flex-end;
`

export const Arrow = styled(Box)`
    display: flex;
    float: right;
    padding: 0px 5px;
`

export const Heading = styled(Typography)`
    font-weight: ${props => props.$bold ? '700' : ''};
    line-height: none;
    margin-block-end: ${props => props.$mbe ? props.$mbe : ''};
    margin-block-start: ${props => props.$mbs ? props.$mbs : ''};
`

export const SLink = styled(Link)`
    text-decoration: none;
`

export const SBox = styled(Box)`
    margin: 10px;
    background: #fff;
`

export const Photo = styled.img`
    margin: ${props => props.$margin ? props.$margin : ''};
`