import { StyledButton } from "./styles";

export default function Button(props) {
  return (
    <StyledButton
    $color={props.$color}
    $padding={props.$padding}
    $margin={props.$margin}
    $background={props.$background}
    $marginl={props.$marginl}
    $float={props.$float}
    onClick={props.onClick}
    $borderRadius={props.$borderRadius}
    type={props.type}
    >{props.children}</StyledButton>
  );
}