import React from "react";
import '../../App.css';
import raketa from '../../assets/raketa.svg';
import music from '../../assets/music.svg';
import karona from '../../assets/karona.svg';
import stv from '../../assets/stv.svg';
import education from '../../assets/📚 Education.png';
import worke from '../../assets/WorkExperience.svg';
import {Wrapper, WorkExperience, Arrow, Education, Heading, Photo} from './styles';
import {Grid} from '@mui/material';

const Counter = () => {
    const allworkex = [
        {
          img: stv,
          logo: raketa,
          title: 'SpaceFleet',
          text: 'Senior Product Designer',
          date: '• April 2019 - Current'
        },
        {
          img: stv,
          logo: music,
          title: 'MusicMash',
          text: 'Information Architect',
          date: '• April 2016 - May 2017'
        },
        {
          img: stv,
          logo: karona,
          title: 'Kingdom',
          text: 'UI Designer',
          date: '• April 2016 - May 2017'
        }
      ]

      const alleducat = [
        {
          img: stv,
          title: 'Stanford University',
          text: 'MSc (Human Computer Interaction)',
          date: '• 2013-2015'
        },
        {
          img: stv,
          title: 'MIT Summer School',
          text: 'UX Training Bootcamp',
          date: '• 2013-2014'
        },
        {
          img: stv,
          title: 'California State University',
          text: 'BSc in Software Engineering',
          date: '• 2009-2012'
        }
      ]
    return(
       <Wrapper item xs={12} sx={{flexDirection: {xs: 'column-reverse', md: 'row-reverse'}}}>
        <WorkExperience>
          <Grid display="flex" flexDirection="column">
            <Grid padding="0px 0px 95px 0px">
              <Photo src={worke} alt="logo"/>
            </Grid>
              {allworkex.map(box =>(
                <Grid key={box.title} item xs={12} sx={{width: {xs: '310px', md: '650px'}}} margin="10px 0px" height="100px">
                  <Arrow>
                    <Photo src={box.img} alt="arrow"/>
                  </Arrow>
                  <Grid display="flex" flexDirection="row">
                    <Grid display="flex" flexDirection="column" justifyContent="center">
                      <Photo src={box.logo} alt="logo"/>
                    </Grid>
                    <Grid display="flex" margin="0px 24px" flexDirection="column">
                      <Heading $bold fontSize="24px">{box.title}</Heading>
                      <Heading color="#666666" fontSize="15px">{box.text}</Heading>
                    </Grid>
                  </Grid>
                    <Heading color="#666666" $right $mbe="0px" item sx={{marginBlockStart: {xs: '0px', md: '-30px'}}} fontSize="16px" padding="0px 82px 0px 0px">{box.date}</Heading>
                  <hr/>
                </Grid>
              ))}
          </Grid>  
        </WorkExperience>

        <Education>
          <Photo src={education} alt="logo"/>
          <Grid display="flex" padding="95px 0px" flexDirection="column">
            {alleducat.map(box =>(
              <Grid key={box.title} item xs={12} sx={{width: {xs: '310px', md: '650px'}}} margin="10px 0px" height="100px">
                <Arrow>
                  <Photo src={box.img} alt="arrow"/>
                </Arrow>
                <Grid fontSize="24px" fontWeight="700">
                  <Heading fontWeight="700" fontSize="24px" $mbs="0px" $mbe="5px">{box.title}</Heading>
                </Grid>
                <Heading color="#666666" $right $mbe="0px" $mbs="0px" fontSize="16px" padding="0px 82px 0px 0px" >{box.date}</Heading>
                <Heading color="#666666" fontSize="15px">{box.text}</Heading>
                <hr/>
              </Grid>
            ))}
          </Grid>
        </Education>
      </Wrapper>
    );
};
export default Counter;