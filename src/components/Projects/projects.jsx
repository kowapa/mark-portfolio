import React from "react";
import '../../App.css';
import wallp1 from '../../assets/IMAGE1.png';
import wallp2 from '../../assets/IMAGE2.png';
import wallp3 from '../../assets/IMAGE3.png';
import left from '../../assets/left.svg';
import right from '../../assets/right.svg';
import stel from '../../assets/strelka.svg';
import Button from '../button/button';
import {Projects, TextHolder, Title, Heading, ProjWallp, Arrow, Photo, SLink, SBox} from './styles';
import {Grid} from '@mui/material';

const Counter = () => {
    const projwallp = [
        {
          img: wallp1,
          title: 'BRANDING',
          text: 'Soulful Rebrand',
          btntext: 'View Project',
          btn: stel,
          alt: 'spilled_paint'
        },
        {
          img: wallp2,
          title: 'PRODUCT DESIGN',
          text: 'Datadash Product design',
          btntext: 'View Project',
          btn: stel,
          alt: 'wave_logo'
        },
        {
          img: wallp3,
          title: 'WEB DESIGN',
          text: 'Maize Website Design',
          btntext: 'View Project',
          btn: stel,
          alt: 'spilled_paint'
        }
      ]
    return(
       <Projects id='projects'>
          <TextHolder>
            <Title>
              <Heading color="#B16CEA" $mbe="10px" $bold letterSpacing="4px">PROJECTS</Heading>
            </Title>
              <Button $float="right">View all projects</Button>
              <Heading display="flex" justifyContent="flex-start" item sx={{fontSize: {xs: '35px', md: '54px'}}} flexDirection="column" $bold>I bring results.</Heading>
              <Heading display="flex" justifyContent="flex-start" item sx={{fontSize: {xs: '35px', md: '54px'}}} flexDirection="column" $bold>My clients are proof.</Heading>
          </TextHolder>
          <ProjWallp>
          {projwallp.map(box => (
            <SBox key={box.title} item xs={12} sx={{display: {xs: 'none', md: 'block'}}}>

                <Photo src={box.img} alt={box.alt}/>

                <Heading fontSize="15px" padding="20px 25px 0px 25px" color="#3C46FF" $bold letterSpacing="4px">{box.title}</Heading>

                <Heading $bold fontSize="24px" padding="0px 0px 0px 25px" $mbs="15px" $mbe="15px">{box.text}</Heading>

              <Grid padding="0px 25px 20px 25px">
                <SLink color="#000" href='/'>{box.btntext}<Photo $margin="0px 0px 0px 10px" src={box.btn} alt="arrow"/></SLink>
              </Grid>
            </SBox>
          ))}

          </ProjWallp>
      </Projects>
    );
};
export default Counter;