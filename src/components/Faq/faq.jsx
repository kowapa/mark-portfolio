import React from "react";
import '../../App.css';
import {StyledAccordion} from '../accordion/accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import vniz from '../../assets/vniz.svg';
import {Wrapper, TextHolder, Heading} from './styles';
import {Grid} from '@mui/material';

const Counter = () => {
    const allquestr = [
        {
          img: vniz,
          text: 'How do you charge for projects?'
        },
        {
          img: vniz,
          text: 'What does your design process look like?'
        },
        {
          img: vniz,
          text: 'What metrics do you use to measure success?'
        },
        {
          img: vniz,
          text: 'What if I need help after the project is complete?'
        }
      ]
    const allquestl = [
        {
          img: vniz,
          text: 'What type of projects do you take on?'
        },
        {
          img: vniz,
          text: 'What is your hourly rate?'
        },
        {
          img: vniz,
          text: 'What time-zone do you work in?'
        },
        {
          img: vniz,
          text: 'What is the typical timeline for a project?'
        }
      ]
    return(
      <Wrapper>
        <TextHolder>

            <Heading $mbs="0px" $mbe="0px" color="#FF8A56" fontSize="22px" $bold letterSpacing="4px">FAQ</Heading>

            <Heading $mbs="0px" $mbe="0px" color="#fff" fontSize="54px" $bold>Frequently asked questions</Heading>

        </TextHolder>
        <Grid padding="100px 0px">
          <StyledAccordion right>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header">
            <Typography><Heading $bold fontSize="24px">What type of projects do you take on?</Heading></Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
              <Heading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                malesuada lacus ex, sit amet blandit leo lobortis eget.
              </Heading>
              </Typography>
            </AccordionDetails>
          </StyledAccordion>

          <StyledAccordion>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header">
            <Typography><Heading $bold fontSize="24px">How do you charge for projects ?</Heading></Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
              <Heading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                malesuada lacus ex, sit amet blandit leo lobortis eget.
              </Heading>
              </Typography>
            </AccordionDetails>
          </StyledAccordion>

          <StyledAccordion right>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header">
            <Typography><Heading $bold fontSize="24px">What is your hourly rate?</Heading></Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
              <Heading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                malesuada lacus ex, sit amet blandit leo lobortis eget.
              </Heading>
              </Typography>
            </AccordionDetails>
          </StyledAccordion>

          <StyledAccordion>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header">
            <Typography><Heading $bold fontSize="24px">What does your design process look like?</Heading></Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
              <Heading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                malesuada lacus ex, sit amet blandit leo lobortis eget.
              </Heading>
              </Typography>
            </AccordionDetails>
          </StyledAccordion>

          <StyledAccordion right>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header">
            <Typography><Heading $bold fontSize="24px">What time-zone do you work in?</Heading></Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
              <Heading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                malesuada lacus ex, sit amet blandit leo lobortis eget.
              </Heading>
              </Typography>
            </AccordionDetails>
          </StyledAccordion>

          <StyledAccordion>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header">
            <Typography><Heading $bold fontSize="24px">What metrics do you use to measure success?</Heading></Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
              <Heading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                malesuada lacus ex, sit amet blandit leo lobortis eget.
              </Heading>
              </Typography>
            </AccordionDetails>
          </StyledAccordion>

          <StyledAccordion right>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header">
            <Typography><Heading $bold fontSize="24px">What is the typical timeline for a project?</Heading></Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
              <Heading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                malesuada lacus ex, sit amet blandit leo lobortis eget.
              </Heading>
              </Typography>
            </AccordionDetails>
          </StyledAccordion>

          <StyledAccordion>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header">
            <Typography><Heading $bold fontSize="24px">What if I need help after the project is complete?</Heading></Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
              <Heading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
                malesuada lacus ex, sit amet blandit leo lobortis eget.
              </Heading>
              </Typography>
            </AccordionDetails>
          </StyledAccordion>
        </Grid>
      </Wrapper>
    );
};
export default Counter;