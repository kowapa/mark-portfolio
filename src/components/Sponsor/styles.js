import styled from 'styled-components';
import {Box, Typography} from '@mui/material';

export const Sponsor = styled(Box)`
    padding: 70px;
`

export const TextHolder = styled(Box)`
    display: flex;
    justify-content: center; 
    align-items: center;
`

export const ImgHolder = styled(Box)`
    display: flex;
    justify-content: center;
    align-items: center; 
    padding: 20px;
`

export const Heading = styled(Typography)`
    font-size: 18px;
    margin-block-start: 15px;
    margin-block-end: 15px;
`

export const Photo = styled.img`
    padding: 0px 55px;
`